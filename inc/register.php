<?php
require("Db.class.php");

// Replace this with your own email address
$siteOwnersEmail = 'no-reply@mundotoa.co';


if($_POST) {
   $email = trim(stripslashes($_POST['email']));
   $pw1 = trim(stripslashes($_POST['pw1']));
   $pw2 = trim(stripslashes($_POST['pw2']));
   // Check Name
	if (strlen($pw1) < 6) {
		$error['pw'] = "La contraseña debe tener al menos 6 caracteres.";
	}
	// Check Email
	if (!preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is', $email)) {
		$error['email'] = "Ingrese un correo válido.";
	}
	// Check Message
	if ($pw1 != $pw2) {
		$error['pw_mismatch'] = "Las contraseñas no coinciden.";
	}


   if (!$error) {

      $db = new Db();
	  if( ($db->query("SELECT COUNT(*) FROM preview_users WHERE email=:email",array("email"=>$email),"PDO::FETCH_NUM")) > 0){
		  $error['already_exists']= "Usuario registrado, ya formas parte del mundo TOA :-)";
	  }else{
	  $insert   =  $db->query("INSERT INTO preview_users(email,password) VALUES(:f,:age)", array("f"=>$email,"age"=>md5($pw1)));
	  echo("OK");

   // Set Message
   $message .= "<img src='http://mundotoa.co/bienvenido.png'/>";

$subject="Bienvenido a TOA (Preregistro)";
   // Set From: header
   $from =   "TOA Inc. <".$siteOwnersEmail.">";

   // Email Headers
	$headers = "From: " . $from . "\r\n";
	$headers .= "Reply-To: ". $siteOwnersEmail . "\r\n";
 	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


      ini_set("sendmail_from", $siteOwnersEmail); // for windows server
      $mail = mail($email, $subject, $message, $headers);


	  }
	} # end if - no validation error

	else {
	 if(!isset($error['already_exists'])){
		$response = (isset($error['email'])) ? $error['email'] . "<br /> \n" : null;
		$response .= (isset($error['pw_mismatch'])) ? $error['pw_mismatch'] . "<br /> \n" : null;
		$response .= (isset($error['pw'])) ? $error['pw'] . "<br />" : null;

		echo $response;
	 }else{
		 echo $error['already_exists']."<br />";
	 }

	} # end if - there was a validation error

}

?>
