<?php
require '../vendor/autoload.php';
// Replace this with your own email address
$siteOwnersEmail = 'toaincgye@gmail.com';


if($_POST) {
   $name = trim(stripslashes($_POST['contactName']));
   $email = trim(stripslashes($_POST['contactEmail']));
   $subject = trim(stripslashes($_POST['contactSubject']));
   $contact_message = trim(stripslashes($_POST['contactMessage']));

   // Check Name
	if (strlen($name) < 2) {
		$error['name'] = "Por favor ingrese su nombre";
	}
	// Check Email
	if (!preg_match('/^[a-z0-9&\'\.\-_\+]+@[a-z0-9\-]+\.([a-z0-9\-]+\.)*+[a-z]{2}/is', $email)) {
		$error['email'] = "Ingrese un correo válido.";
	}

	// Check Message
	if (strlen($contact_message) < 15) {
		$error['message'] = "Por favor ingrese su mensaje, debe tener al menos 15 caracteres.";
	}
   // Subject
	if ($subject == '') { $subject = "Contact Form Submission"; }

  $mail = new PHPMailer;


  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'toa.westus.cloudapp.azure.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'radmintoa';                 // SMTP username
  $mail->Password = 'Admintro1';                           // SMTP password
  $mail->Port = 25;                                    // TCP port to connect to

  $mail->setFrom($email, $name);
  $mail->addAddress($siteOwnersEmail);     // Add a recipient
  $mail->addReplyTo($email, $name);

  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = $subject;
  $mail->Body    = $contact_message;
  $mail->AltBody = $contact_message;



   if (!$error) {


      if(!$mail->send()) {
          echo 'Message could not be sent.';
      } else {
          echo 'OK';
      }

	} # end if - no validation error

	else {

		$response = (isset($error['name'])) ? $error['name'] . "<br /> \n" : null;
		$response .= (isset($error['email'])) ? $error['email'] . "<br /> \n" : null;
		$response .= (isset($error['message'])) ? $error['message'] . "<br />" : null;

		echo $response;

	} # end if - there was a validation error

}else{
  echo "Resource unavailable.";
}

?>
