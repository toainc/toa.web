<?php

/**
 * Representa el la estructura de los consejos
 * almacenadas en la base de datos
 */
require 'Database.php';

class Consejo
{
    function __construct()
    {
    }

    /**
     * Retorna en la fila especificada de la tabla 'meta'
     *
     * @param $idMeta Identificador del registro
     * @return array Datos del registro
     */
    public static function getAll()
    {
        $consulta = "SELECT * FROM meta";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
echo $e;
            return false;
        }
    }

    /**
     * Obtiene los campos de un consejo con un identificador
     * determinado
     *
     * @param $idConsejo Identificador del consejo
     * @return mixed
     */
    public static function getById($idConsejo)
    {
        // Consulta del consejo
        $consulta = "SELECT idMeta,
                            titulo,
                             descripcion,
                             autor
                             fechaLim,
                             categoria
                             FROM meta
                             WHERE idConsejo= ?";

        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($idConsejo));
            // Capturar primera fila del resultado
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;

        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return -1;
        }
    }

    /**
     * Actualiza un registro de la bases de datos basado
     * en los nuevos valores relacionados con un identificador
     *
     * @param $idConsejo   identificador
     * @param $titulo      nuevo titulo
     * @param $descripcion nueva descripcion
     * @param $fechaLim    nueva fecha limite de cumplimiento
     * @param $categoria   nueva categoria
     * @param $autor   	   nuevo autor
     */
    public static function update(
        $idConsejo,
        $titulo,
        $descripcion,
        $fechaLim,
        $categoria,
        $autor
    )
    {
        // Creando consulta UPDATE
        $consulta = "UPDATE meta" .
            " SET titulo=?, descripcion=?, fechaLim=?, categoria=?, autor=? " .
            "WHERE idConsejo=?";

        // Preparar la sentencia
        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        // Relacionar y ejecutar la sentencia
        $cmd->execute(array($titulo, $descripcion, $fechaLim, $categoria, $autor, $idConsejo));

        return $cmd;
    }

    /**
     * Insertar un cosnejo
     *
     * @param $titulo      titulo del nuevo registro
     * @param $descripcion descripción del nuevo registro
     * @param $fechaLim    fecha limite del nuevo registro
     * @param $categoria   categoria del nuevo registro
     * @param $autor   	   autor del nuevo registro
     * @return PDOStatement
     */
    public static function insert(
        $titulo,
        $descripcion,
        $fechaLim,
        $categoria,
        $autor
    )
    {
        // Sentencia INSERT
        $comando = "INSERT INTO meta ( " .
            "titulo," .
            " descripcion," .
            " fechaLim," .
            " categoria," .
            " autor)" .
            " VALUES( ?,?,?,?,?)";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(
            array(
                $titulo,
                $descripcion,
                $fechaLim,
                $categoria,
                $autor
            )
        );

    }

    /**
     * Eliminar el registro con el identificador especificado
     *
     * @param $idConsejo identificador del consejo
     * @return bool Respuesta de la eliminación
     */
    public static function delete($idConsejo)
    {
        // Sentencia DELETE
        $comando = "DELETE FROM meta WHERE idConsejo=?";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(array($idMeta));
    }
}

?>
