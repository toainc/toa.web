<?php
require 'Consejo.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    // Manejar petición GET
    $metas = Consejo::getAll();

    if ($metas) {

        $datos["estado"] = 1;
        $datos["consejo"] = $metas;

header('Content-Type: application/json');
        echo json_encode($datos);
    } else {
        print json_encode(array(
            "estado" => 2,
            "mensaje" => "Ha ocurrido un error"
        ));
    }
}
?>
