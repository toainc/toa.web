$(function(){
	'use strict';
	var $refresh =$('#refresh');
	var $table= $('#table');

	//Formulario
	var $nombre=$('#nombre');
	var $apellido = $('#apellido');
	var $cedula = $('#cedula');
	var $telefono = $('#telefono');
	var $correo = $('#correo');
	var $correo2 = $('#correo2');
	var $fecha_nacimiento = $('#fecha_nacimiento');
	var $direccion = $('#direccion');
	var $celular = $('#celula');
	var $genero= $('#genero');
	var $btnenviar= $('#btnenviar');
	var $btnreportes=$('#btnreportes');

	//template
	var $source = $('#tableTemplate').html();

	function llenarTabla(){
		$.ajax({
			url: 'php/mostrar.php',
			type: 'GET',
		})
		.done(function(response) {
			var template = Handlebars.compile($source);
			response= $.parseJSON(response);
			$table.find('tbody').find('tr').remove();
			$.each(response, function(index, val) {
				var fila = template(val);
				$table.find('tbody').append(fila);
			});

		})
		.fail(function() {

		})
		.always(function() {

		});

	}
	function enviarFormulario(){
		var nombre = $nombre.val();
		var apellido= $apellido.val();
		var cedula	= $cedula.val();
		var telefono= $telefono.val();
		var correo= $correo.val();
		var correo2	= $correo2.val();
		var fecha_nacimiento= $fecha_nacimiento.val();
		var direccion= $direccion.val();
		var celular= $celular.val();
		var genero= $genero.val();
		if( nombre.length > 0 && apellido.length > 0 && cedula.length>0 && telefono.length>0 && correo.length>0 && fecha_nacimiento.length>0 && genero.length>0){
			$.ajax({
				url: 'php/registro.php',
				type: 'POST',
				data: {nombre: nombre,apellido: apellido,cedula: cedula, telefono: telefono, correo:correo ,correo2:correo2, fecha_nacimiento:fecha_nacimiento ,direccion:direccion, celular:celular,genero:genero},
			})
			.done(function(response) {
				$('#div_form').html("<div id='message'></div>");
				$('#message').html('<h5>El usuario se ha registrado con éxito</h5>');
				console.log(response);
			})
			.fail(function() {
				$("input#name").select().focus();
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});


		} else
		alert('llena los campos ');

	}

	$refresh.click(llenarTabla);
	$btnreportes.click(llenarTabla);
	$btnenviar.click(enviarFormulario);

	llenarTabla();
});
