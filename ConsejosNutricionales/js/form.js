(function(){
  'use strict';
  var lenguaje = {requiredFields : "Este campo es necesario"};

  var $userRegistrationForm = $("#userRegistrationForm"),
      $formError = $('.form-error');


  function sendForm(env){
      env.preventDefault();

      setTimeout(function(){
        if($('.form-error').length == 0){
          saveUser($("#userRegistrationForm").serializeArray(),$userRegistrationForm.attr('action'));

        }
      },1000);


  }

  function saveUser(data,action){
    $.ajax({
      'url' : action,
      'data' : data,
      'method' : 'POST',

    }).done(function(response){

      if(response.success === 1){
        $("#userRegistrationForm")[0].reset();
        swal("Usuario registrado", "presione ok para continuar", "success");
      }
    }).fail(function(){

    })
  }


  $userRegistrationForm.on({
    'submit' : sendForm
  })




  $.validate({language : lenguaje});

})();
