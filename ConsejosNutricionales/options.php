<!doctype html>
<html class="no-js" lang="en">
  <head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <title></title>
     <link rel="stylesheet" href="css/foundation.css" />
     <link rel="stylesheet" href="css/style.css" />
     <script src="js/vendor/modernizr.js"></script>
     <!--<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">-->
     <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
     <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
     <link rel="stylesheet" href="css/sweetalert.css">
  </head>
  <body>
      <div class="">
        <div class="large-12 columns TopBar">
          <div class="large-6 columns">

          </div>
          <div class="large-12 small-centered columns">
            <h4 class="title-style"><i class="fa fa-users"></i> Opciones usuario</h4>
          </div>
        </div>
      </div>
      <div class="">
          <div class="SidebarLeft large-3 columns side-style">
            <h1><img class="img-style" src="img/acerimallas1.png"></h1>
            <ul class="side-nav">
              <li>  <a href="options.php?page=message" class="options-styles">Notificaciones</a></li>
              <li>  <a href="options.php?page=form" class="options-styles" >Registro usuario</a></li>
              <li>  <a href="options.php?page=reporte" class="options-styles">Reporte</a></li>
            </ul>
           <!-- <h1><img src="img/logo.png"></h1>-->
          </div>
          <div class="large-9 columns" id="container">
              <?php
                if(isset($_GET['page']))
                {
                  $file = $_GET['page'] .'.php';
                  if(file_exists($file)){
                    require $file;
                  }
                }

              ?>
          </div>
      </div>
      <script src="js/vendor/jquery.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js"></script>
      <script src="js/sweetalert.min.js"></script>
      <script src="js/main.js"></script>
      <script src="js/form.js"></script>
      <script src="js/handlebars-v3.0.3.js"></script>
      <script id="tableTemplate" type="text/x-handlebars-template">
        <tr>
          <td>{{nombre}}</td>
          <td>{{apellido}}</td>
          <td>{{cedula}}</td>
          <td>{{telefono}}</td>
          <td>{{fecha_nacimiento}}</td>
          <td>{{fecha_creacion}}</td>
          <td>{{correo}}</td>
          <td>{{celular}}</td>
          <td>{{direccion}}</td>
          <td>{{genero}}</td>
        </tr>
      </script>

  </body>

</html>
