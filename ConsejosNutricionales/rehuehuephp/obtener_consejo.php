<?php
/**
 * Obtiene todas los consejos de la base de datos
 */

require 'Consejo.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    // Manejar petición GET
    $consejo = Consejo::getAll();

    if ($consejo) {

        $datos["estado"] = 1;
        $datos["consejo"] = $consejo;

        print json_encode($datos);
    } else {
        print json_encode(array(
            "estado" => 2,
            "mensaje" => "Ha ocurrido un error"
        ));
    }
}

