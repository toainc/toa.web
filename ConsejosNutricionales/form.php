<form method="post" action="php/registro.php" id="userRegistrationForm">
  <div class="row">
    <div class="small-12 large-12">
      <h3 class="text-center text-margin"><i class="fa fa-user-plus"></i>Registro de usuario</h3>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="nombre" class="right inline">Nombre</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="nombre" data-validation="required" name="nombre">
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="email" class="right inline">Correo</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="email" name="email">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="apellido" class="right inline">Apellido</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="apellido" name="apellido" data-validation="required">
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="correo2" class="right inline">Correo 2</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="correo2" name="correo2">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="cedula" class="right inline">Cédula</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="cedula" name="cedula">
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="celular" class="right inline">Celular</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="celular" name="celular" data-validation="required">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="movil" class="right inline">Teléfono</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="movil" name="movil"  >
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="direccion" class="right inline">Dirección</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="direccion" name="direccion" data-validation="required">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="empresa" class="right inline">Empresa</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="text" id="empresa" name="empresa">
        </div>
      </div>
    </div>
    <div class="small-12 large-6 columns">
       <div class="row">
        <div class="small-3 columns">
          <label for="fecha_nacimiento" class="right inline">Fecha de nacimiento</label>
        </div>
        <div class="small-9 columns">
          <input class="input-style" type="date" id="fecha_nacimiento" name="fecha_nacimiento">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="" class="right inline">Genero</label>
        </div>
        <div class="small-9 columns">
          <div class="small-12 large-6 columns">
            <label class="lbl-genero">
              Masculino <input  type="radio" name="genero" value="M" class="inline">
            </label>
          </div>
          <div class="small-12 large-6 columns">
            <label class="lbl-genero">
              Femenino <input  type="radio" name="genero" value="F" class="inline">
            </label>
          </div>
        </div>
      </div>
    <div class="small-12 large-6 columns">
      <div class="row">
        <div class="small-3 columns">
          <label for="fecha_nacimiento" class="right inline"></label>
        </div>
        <div class="small-9 columns"></div>
      </div>
    </div>

    </div>
  </div>
  <div class="row">
     <div class="large-6 columns"></div>
     <div class="large-6 columns">
       <input type="reset" class="button right space" value="Limpiar">
       <button class="button right space">Enviar</button>
     </div>
  </div>
</form>
