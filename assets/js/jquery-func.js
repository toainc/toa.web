// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

   $('form#contactForm button.submit').click(function() {

      $('#image-loader').fadeIn();

      var contactName = $('#contactForm #contactName').val();
      var contactEmail = $('#contactForm #contactEmail').val();
      var contactSubject = $('#contactForm #contactSubject').val();
      var contactMessage = $('#contactForm #contactMessage').val();

      var data = 'contactName=' + contactName + '&contactEmail=' + contactEmail +
               '&contactSubject=' + contactSubject + '&contactMessage=' + contactMessage;

      $.ajax({

	      type: "POST",
	      url: "inc/sendEmail.php",
	      data: data,
	      success: function(msg) {

            // Message was sent
            if (msg == 'OK') {
               $('#image-loader').fadeOut();
               $('#message-warning').hide();
               $('#contactForm').fadeOut();
               $('#message-success').fadeIn();
            }
            // There was an error
            else {
               $('#image-loader').fadeOut();
               $('#message-warning').html(msg);
	            $('#message-warning').fadeIn();
            }

	      }

      });
      return false;
   });
 $('form#registerForm button.submit').click(function() {

      $('#image-loader').fadeIn();

      var email = $('#registerForm #Email').val();
      var pw1 = $('#registerForm #password').val();
      var pw2 = $('#registerForm #password2').val();

      var data = 'email=' + email + '&pw1=' + pw1 +
               '&pw2=' + pw2;

      $.ajax({

	      type: "POST",
	      url: "inc/register.php",
	      data: data,
	      success: function(msg) {

               $('#message-warning2').html(msg);
	            $('#message-warning2').fadeIn();
            // Message was sent
            if (msg == 'OK') {
               $('#image-loader2').fadeOut();
               $('#message-warning2').hide();
               $('#registerForm').fadeOut();
               $('#message-success2').fadeIn();
            }
            // There was an error
            else {
               $('#image-loader2').fadeOut();
               $('#message-warning2').html(msg);
	            $('#message-warning2').fadeIn();
            }

	      }

      });
      return false;
   });
